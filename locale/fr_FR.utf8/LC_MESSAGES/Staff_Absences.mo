��          �      |      �     �     �                 	   +     5     E     W     m     ~     �     �     �     �  5   �     �       &        A  3   `  �  �     �     �     �     �     �     �     �     �     �          4     @     F     S     i  9   �     �     �  '   �  )     6   @                               
                                       	                           Absence Absence Field Absence Fields Absences Add Absence Afternoon Cancelled Class Cancelled Classes Days Absent Breakdown Email Absence to End Date Morning My Absences New Absence Field New staff absence Parents having students enrolled in cancelled classes Please choose a field Staff Absences Students enrolled in cancelled classes That absence has been emailed. That absence has been referred to an administrator. Project-Id-Version: Staff Absences module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:39+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Absence Champ absence Champs absence Absences Ajouter une absence Après-midi Classe annulée Classes annulées Répartition jours d'absence Envoyer l'absence par email à Date de fin Matin Mes absences Nouveau champ absence Nouvelle absence du personnel Parents ayant des élèves inscrits aux classes annulées Merci de choisir un champ Absences personnel Élèves inscrits aux classes annulées Cette absence a été envoyée par email. Cette absence a été rapportée à un administrateur. 