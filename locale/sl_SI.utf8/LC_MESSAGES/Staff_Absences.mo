��          �      |      �     �     �                 	   +     5     E     W     m     ~     �     �     �     �  5   �     �       &        A  3   `  �  �  	   �     �     �  
   �     �     �     �     �               3     E     N     ^     v  7   �     �     �  &   �  )     *   >                               
                                       	                           Absence Absence Field Absence Fields Absences Add Absence Afternoon Cancelled Class Cancelled Classes Days Absent Breakdown Email Absence to End Date Morning My Absences New Absence Field New staff absence Parents having students enrolled in cancelled classes Please choose a field Staff Absences Students enrolled in cancelled classes That absence has been emailed. That absence has been referred to an administrator. Project-Id-Version: Staff Absences module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:39+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Odsotnost Polje odsotnosti Polja odsotnosti Odsotnosti Dodaj Odsotnost Popoldne Preklicana učna ura Preklicane učna ure Število Dni Odsotnosti Odsotnost po e-pošti na Končaj z datumom Dopoldne Moje odsotnosti Novo polje za odsotnost Nova odsotnost osebja Starši, ki imajo dijake vpisane v odpovedane učne ure Prosimo izberite polje Odsotnosti osebja Dijaki, vpisani v odpovedano učno uro Ta odsotnost je bila poslana po e-pošti. Ta odsotnost je bila posredovana skrbniku. 