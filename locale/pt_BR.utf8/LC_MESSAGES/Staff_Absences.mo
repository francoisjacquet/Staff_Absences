��          �      l      �     �     �     �       	             ,     >     T     e     n     v     �     �  5   �     �     �  &        (  3   G  �  {  	   t     ~     �     �     �     �     �  !   �       
        &     -     ?     W  1   v     �     �  (   �  &   	  5   0                               	                     
                                              Absence Absence Field Absence Fields Add Absence Afternoon Cancelled Class Cancelled Classes Days Absent Breakdown Email Absence to End Date Morning My Absences New Absence Field New staff absence Parents having students enrolled in cancelled classes Please choose a field Staff Absences Students enrolled in cancelled classes That absence has been emailed. That absence has been referred to an administrator. Project-Id-Version: Staff Absences module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:39+0200
Last-Translator: Emerson Barros
Language-Team: RosarioSIS <info@rosariosis.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Ausência Campo de ausência Campos de ausência Adicionar ausência Tarde Turma cancelada Turmas canceladas Detalhamento de dias de ausência Email de ausência para Data final Manhã Minhas ausências Novo campo de ausência Nova ausência de funcionário Pais com alunos matriculados em turmas canceladas Por favor, escolha um campo Ausências de funcionários Alunos matriculados em turmas canceladas Essa ausência foi enviada por e-mail. Essa ausência foi encaminhada para um administrador. 