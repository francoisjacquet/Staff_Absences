/**
 * Add Absence program JS
 *
 * @package Staff Absences module
 */

if ($('#search').length) {
	// Remove parent & none profiles from list.
	$("#profile option[value='parent']").remove();
	$("#profile option[value='none']").remove();
	$("input[name='include_inactive']").parent( 'label' ).remove();
}
